The purpose of this tutorial is to fragment a 1D bar (made of 2D triangle elements) and to study the
influence of several parameters controling the fragmentation process

Important parameters are:

- material properties (Youngs modulus, poisson ratio, density)
- Geometry (size of the bar, size of elements)
- Strain rate (Initial velocity profile and maintained pulling velocity)
- Randomization of critical stress
- Contact (computes collision forces between formed fragments)

Time variation graphs of energy, stress and number of fragments will be shown.

The final distribution of fragments will also be shown
