# Tutorial dependencies

In order to run these tutorials, you have to install the dependencies:

```sh
apt install libgl1-mesa-dev libglu1-mesa-dev ffmpeg
pip install -r requirements.txt
```

# Akantu tutorials repository on Renku

[![launch - renku](https://renkulab.io/renku-badge.svg)](https://renkulab.io/projects/guillaume.anciaux/akantu-tutorials/sessions/new?autostart=1)

