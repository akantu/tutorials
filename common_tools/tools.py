from tqdm.notebook import tqdm
import os
import re
import akantu as aka
import pyvista as pv
from Slides import presentation_helper as ph
# from ipywidgets import FloatProgress
# from IPython.display import display
from contextlib import contextmanager
import matplotlib.pyplot as plt

################################################################


@contextmanager
def make_figure(equal_ratio=False):

    fig = plt.figure()
    axe = fig.add_subplot(111)
    if equal_ratio:
        axe.set_aspect('equal')
    try:
        yield fig, axe
    finally:
        plt.show()


################################################################

def get_paraview_frames(name):
    files = []
    for f in os.listdir('paraview'):
        if os.path.splitext(f)[1] != '.pvtu':
            continue
        m = re.match(f'{name}_(.*).pvtu', f)
        if m:
            files.append((int(m[1]), os.path.join('paraview', f)))
    files = sorted(files, key=lambda x: x[0])
    return files

################################################################


def make_video(names, generate_frame, framerate=25, width="100%", **kwargs):

    if isinstance(names, str):
        names = [names]

    frames = []
    for name in names:
        frames.append(get_paraview_frames(name))
    files = []
    camera = None

    frames = [e for e in zip(*frames)]
    for frame in tqdm(frames):
        p = pv.Plotter(off_screen=True, notebook=False)
        if camera is not None:
            p.camera = camera
        pv_files = [pv.read(f) for n, f in frame]
        step_count = [n for n, f in frame]
        filenames = [f for n, f in frame]
        if len(pv_files) == 1:
            generate_frame(pv_files[0], step=step_count,
                           plotter=p, camera=camera, **kwargs)
        else:
            generate_frame(dict(zip(names, pv_files)), step=step_count,
                           plotter=p, camera=camera, **kwargs)

        img_fname = os.path.splitext(filenames[0])[0] + '.png'
        img = p.show(screenshot=img_fname)

        if step_count[0] == 0:
            camera = p.camera.copy()
        files.append(img_fname)
    return ph.makeAnimationFromImageFiles(files, framerate=framerate,
                                          width=width)

################################################################


def gmsh_to_akantu_mesh(gmsh):
    _msh_to_akantu_element_types = [
        aka._not_defined,
        aka._segment_2,
        aka._triangle_3,
        aka._quadrangle_4,
        aka._tetrahedron_4,
        aka._hexahedron_8,
        aka._pentahedron_6,
        aka._not_defined,
        aka._segment_3,
        aka._triangle_6,
        aka._not_defined,
        aka._tetrahedron_10,
        aka._not_defined,
        aka._not_defined,
        aka._not_defined,
        aka._point_1,
        aka._quadrangle_8,
        aka._hexahedron_20,
        aka._pentahedron_15,
    ]

    gmsh_mesh = gmsh.model.mesh
    types = gmsh_mesh.get_element_types()
    dim = -1
    for el_type in types:
        props = gmsh_mesh.get_element_properties(el_type)
        dim = max(dim, props[1])
    aka_mesh = aka.Mesh(dim)
    accessor = aka.MeshAccessor(aka_mesh)

    gmsh_nodes = gmsh_mesh.get_nodes()

    nb_nodes = gmsh_nodes[0].shape[0]
    accessor.resizeNodes(nb_nodes)

    aka_nodes = aka_mesh.getNodes()
    gmsh_aka_nodes = {}
    for i in range(nb_nodes):
        aka_nodes[i, :] = gmsh_nodes[1][i*3:i*3+dim]
        gmsh_aka_nodes[gmsh_nodes[0][i]] = i

    gmsh_aka_elements = {}
    for el_type in types:
        aka_type = _msh_to_akantu_element_types[el_type]
        gmsh_aka_elements[aka_type] = {}
        tags, gmsh_conn = gmsh_mesh.get_elements_by_type(el_type)
        aka_mesh.addConnectivityType(aka_type)
        accessor.resizeConnectivity(tags.shape[0], aka_type)
        conn = aka_mesh.getConnectivity(aka_type)

        for el, tag in enumerate(tags):
            gmsh_aka_elements[aka_type][tag] = el
            nnodes_per_el = conn.shape[1]
            for n in range(nnodes_per_el):
                conn[el, n] = gmsh_aka_nodes[gmsh_conn[el * nnodes_per_el + n]]

    for group in gmsh.model.get_physical_groups():
        name = gmsh.model.get_physical_name(*group)
        types = gmsh_mesh.get_element_types(*group)
        aka_group = aka_mesh.createElementGroup(
            name, spatial_dimension=group[0], replace_group=True)
        entities = gmsh.model.get_entities_for_physical_group(*group)
        for el_type in types:
            aka_type = _msh_to_akantu_element_types[el_type]
            for entity in entities:
                elements = gmsh_mesh.get_elements_by_type(el_type, tag=entity)
                for el in elements[0]:
                    aka_group.add(aka.Element(
                        aka_type, gmsh_aka_elements[aka_type][el],
                        aka._not_ghost), True, False)
        aka_group.optimize()
    accessor.makeReady()
    return aka_mesh

################################################################
