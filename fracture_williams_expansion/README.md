### Stress intensity factor

The main goal of this tutorial is to compute the stress intensity factor in an edge, mode-I, crack configuration.
The stress and displacement profiles are extracted from an isotrope linear elastic calculation.
The *Williams* series are fitted close to the crack tip to show the classical inverse *square-root* singularity.
The effect of mesh refinment is also highlighted in this tutorial.
