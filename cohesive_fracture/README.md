The main goal of this tutorial is to model a crack propagation with extrinsic cohesive elements.
An edge crack is pre-loaded with an isotrope linear elastic constitutive law with a concentrated load
beyond the critical stress. Then a dynamical simulation is conducted with dynamic insertion, leading to 
a crack propagation. 

It is shown how to produce a 3D view of the model as well as a video of the dynamics with *PyVista* and *ffmpeg*.
