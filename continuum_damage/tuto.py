#!/usr/bin/env python3
import gmsh

gmsh.initialize()


def generateMesh(Lx, Ly, nx, ny, **kwargs):
    gmsh.model.add("bar")
    gmsh.model.occ.addRectangle(-Lx/2., -Ly/2, 0, Lx, Ly, 0)
    gmsh.model.occ.synchronize()
    lines = gmsh.model.get_entities(dim=1)
    for entity, name in zip(lines, ["bottom", "right", "top", "left"]):
        gmsh.model.add_physical_group(1, [entity[1]], entity[1])
        gmsh.model.set_physical_name(1, entity[1], name)
        if name in ["bottom", "top"]:
            gmsh.model.mesh.set_transfinite_curve(
                entity[1], nx+1, "Progression", 1)
        else:
            gmsh.model.mesh.set_transfinite_curve(
                entity[1], ny+1, "Progression", 1)
    bar = gmsh.model.get_entities(dim=2)
    gmsh.model.add_physical_group(2, [bar[0][1]], bar[0][1])
    gmsh.model.set_physical_name(2, bar[0][1], "bulk")
    gmsh.model.mesh.set_transfinite_surface(bar[0][1])
    gmsh.model.mesh.set_recombine(2, bar[0][1])
    gmsh.model.mesh.generate(2)
    gmsh.write("bar.msh")
    return gmsh


def write_material(E, rho, nu, ε_p, **kwargs):
    Yd = 1. / 2. * ε_p * E * ε_p
    material_file = f"""
material marigo [
        name = virtual
        rho = {rho} # density
        E   = {E}   # young's modulus
        nu  = {nu}  # poisson's ratio
        Yd  = {Yd}
        Sd  = 1e2
]
"""

    with open('material.dat', 'w') as f:
        f.write(material_file)
