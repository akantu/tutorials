In this tutorial, a bar is stretched from its external open boundaries, at a fixed strain rate.
The emited P-waves will meet at the center and create damage. With this model the damage region depends on the mesh characteristic size.
The refinement of the mesh is controlable, so that the localization occuring from the local damage formulation of Marigo can be demonstrated.
