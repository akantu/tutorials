#! /bin/env python

import streamlit as st
import os
import fnmatch
import solidipes as sp
import subprocess

subprocess.call("git lfs fetch --all", shell=True)
################################################################

st.set_page_config(layout="wide")
# try:
#     session = os.environ["SESSION_URL"]
#     tmp = session.split('?')[0]
#     if tmp:
#         session = tmp
#
#     st.write(session)
# except Exception:
#     pass
################################################################


def get_jupyter_link(f):
    try:
        session = os.environ["SESSION_URL"]
        tmp = session.split('?')[0]
        if tmp:
            session = tmp
        _link = f"{session}/lab/tree/{f}"
        return _link
    except Exception:
        pass

################################################################


def display_tutorial(f):
    excluded_patterns = ['__pycache__', '.*', 'paraview', '*~']

    files = []
    for _f in os.listdir(f):
        excluded = False
        for p in excluded_patterns:
            if fnmatch.fnmatch(_f.strip(), p.strip()):
                excluded = True
        if excluded:
            continue
        files.append(_f)

    file_list = ""
    for _f in files:
        file_list += '- ' + _f + "\n"

    highlights = [os.path.join(f, _f) for _f in files if os.path.splitext(_f)[
        0] == 'highlight']

    col1, col2 = st.columns(2)

    readme = os.path.join(f, 'README.md')
    schematic = os.path.join(f, 'schematic.svg')

    if os.path.exists(readme):
        col1.markdown(open(readme).read())
    else:
        col1.error('Missing README')
    if highlights:
        with col2:
            s = sp.load_file(highlights[0])
            s.view()
    elif os.path.exists(schematic):
        with col2:
            s = sp.load_file(schematic)
            s.view()
    else:
        col2.error('Missing highlight')

    if os.path.isfile(f'{f}/{f}.ipynb'):
        url = get_jupyter_link(f)
        if url:
            col1.markdown(
                f"[Open in Jupyterlab]({url}/{f}.ipynb)",
                unsafe_allow_html=True)
    else:
        col1.error(f'Missing {f}/{f}.ipynb')
    st.markdown('<br>', unsafe_allow_html=True)

    with st.expander('File list'):
        st.markdown(file_list)

################################################################
# icon and header


aka_icon = "https://gitlab.com/uploads/-/system/group/avatar/6660847/profile.png?width=64"
st.markdown(
    f'# <img src="{aka_icon}" alt="A"></img>kantu Tutorials', unsafe_allow_html=True)
st.markdown('#### Please find below the full list of *Akantu* tutorials.')
st.markdown(
    'GitLab Repository: https://renkulab.io/gitlab/guillaume.anciaux/akantu-tutorials')
st.markdown('All tutorials are coded in *Python* within a *Jupyter Notebook*')
################################################################
tutorials = {
    "Modeling a plate with a hole": "plate_hole",
    "Make your own constitutive law (example of a Marigo/damage law)": "constitutive_laws",
    "Propagation of a P-Wave in a plate": "wave_propagation",
    "Edge-crack propagation with cohesive element dynamical insertion (extrinsic)": "cohesive_fracture",
    "Localization of damage using the continuum damage model of *Marigo*": "continuum_damage",
    "Structural stability of the EPFL Logo": "epfl_logo",
    "Edge-crack characterization of stress intensity factors by fitting *Williams* series": "fracture_williams_expansion",
    "Fragmentation in a 1D bar with cohesive elements": "fragmentation_cohesive",
#    "Fracture simulation with PhaseField": "phase_field",
}

for k, f in tutorials.items():
    with st.container():
        st.markdown('---')
        st.markdown(f'# {k}')
        display_tutorial(f)
