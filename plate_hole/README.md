In this tutorial we will describe how to employ **Akantu** to compute the displacement and stress fields within a loaded plate with a hole. 
The hole will play the role of the stress concentrator, as classically described in textbooks. Isotropic linear elasticity will be employed in 
this simulation. Output with matplolib will be demonstrated.

