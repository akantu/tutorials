In this tutorial a P-wave is sent to propagate through a plate without dissipation.

This demonstrates the explicit time step possibilities of *Akantu*
