import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.font_manager as font_manager
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import ticker


mpl.rcParams['font.family'] = 'serif'
mpl.rcParams["font.serif"] = ["Times New Roman"] + mpl.rcParams["font.serif"]
mpl.rcParams['font.stretch'] = 'condensed'

mpl.rcParams['font.size'] = 9
mpl.rcParams['legend.fontsize'] = 8
mpl.rcParams['savefig.format'] = 'pdf'
mpl.rcParams['mathtext.fontset'] = 'cm'


mpl.rcParams['xtick.major.width'] = 0.5
mpl.rcParams['xtick.minor.width'] = 0.5

mpl.rcParams['ytick.major.width'] = 0.5
mpl.rcParams['ytick.minor.width'] = 0.5

mpl.rcParams['axes.linewidth'] = 0.5 # set the value globally



mpl.rcParams['figure.dpi'] = 150
mpl.rcParams['savefig.dpi'] = 200
mpl.rcParams['savefig.transparent'] = True

def set_size(fraction=0.5, width=455.24411):
    fig_width_pt = width * fraction
    inches_per_pt = 1.0 / 72.27
    golden_ratio = (np.sqrt(5) - 1.0) / 2.0
    fig_width_in = fig_width_pt * inches_per_pt
    fig_height_in = fig_width_in * golden_ratio
    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim

mpl.rcParams['figure.figsize'] = set_size(fraction=0.75)
mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['ytick.direction'] = 'in'


class OOMFormatter(mpl.ticker.ScalarFormatter):
    def __init__(self, order=0, fformat="%1.1f", offset=True, mathText=True):
        self.oom = order
        self.fformat = fformat
        mpl.ticker.ScalarFormatter.__init__(self,useOffset=offset,useMathText=mathText)
    def _set_order_of_magnitude(self):
        self.orderOfMagnitude = self.oom
    def _set_format(self, vmin=None, vmax=None):
        self.format = self.fformat
        if self._useMathText:
             self.format = r'$\mathdefault{%s}$' % self.format
