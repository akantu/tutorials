#!/bin/env python

# import akantu
import akantu as aka
# import numpy for vector manipulation
import numpy as np
# import the pyplot submodule to draw figures
import matplotlib.pyplot as plt
# import triangluation routine to plot meshes
import matplotlib.tri as tri
# setting a default image size large enough
from plot_helper import *
from tqdm.notebook import tqdm
import subprocess


def createMesh(h1=2.5e-4, h2=7.5e-4, L=32e-3,
               H=16e-3, _l=4e-3, quiet=False):
    geometry_file = f"""
    h1 = {h1};
    h2 = {h2};
    L = {L};
    H = {H};
    l = {_l};
    """ + """
    Point(1) = {0, 0, 0, h1};
    Point(2) = {L, 0, 0, h1};
    Point(3) = {L, H/2, 0, h2};
    Point(4) = {0, H/2, 0, h2};
    Point(5) = {l, 0, 0, h1};

    Point(6) =  {0, 0, 0, h1};
    Point(7) =  {L, -H/2, 0, h2};
    Point(8) =  {0, -H/2, 0, h2};


    Line(1) = {1, 5};
    Line(2) = {4, 1};
    Line(3) = {3, 4};
    Line(4) = {2, 3};
    Line(5) = {5, 2};

    Line Loop(1) = {2, 3, 4, 5, 1};
    Plane Surface(1) = {1};

    Line(6) =  {5, 6};
    Line(7) =  {6, 8};
    Line(8) =  {8, 7};
    Line(9) =  {7, 2};
    Line Loop(2) = {6, 7, 8, 9, -5};
    Plane Surface(2) = {2};


    Physical Surface(8) = {1,2};


    Physical Line("left") = {2,7};
    Physical Line("bottom") = {8};
    Physical Line("top") = {3};
    Physical Line("right") = {4,9};

    """

    with open('plate.geo', 'w') as f:
        f.write(geometry_file)

    p = subprocess.Popen(
        "gmsh -2 -order 1 -o plate.msh plate.geo",
        shell=True, stdout=subprocess.PIPE)
    if p.returncode:
        print("Beware, gmsh could not run: mesh is not regenerated")
    elif quiet is False:
        print("Mesh generated")

    spatial_dimension = 2
    mesh = aka.Mesh(spatial_dimension)
    mesh.read('plate.msh')
    return mesh

################################################################


def createElasticModel(**kwargs):
    mesh = createMesh(**kwargs)

    material_file = """
material elastic [
    name = virtual
    rho = 1    # density
    E   = 1    # young's modulus
    nu  = 0.3  # poisson's ratio
    finite_deformation = false
]
"""

    with open('material.dat', 'w') as f:
        f.write(material_file)

    # reading material file
    aka.parseInput('material.dat')

    # creating mesh
    spatial_dimension = 2
    mesh = aka.Mesh(spatial_dimension)
    mesh.read('plate.msh')

    # creating the model
    model = aka.SolidMechanicsModel(mesh)
    model.initFull(_analysis_method=aka._static)

    # configure the solver
    solver = model.getNonLinearSolver('static')
    solver.set('max_iterations', 100)
    solver.set('threshold', 1e-10)
    solver.set("convergence_type", aka.SolveConvergenceCriteria.residual)

    # Dirichlet
    model.applyBC(aka.FixedValue(0., aka._x), 'left')
    model.applyBC(aka.FixedValue(0., aka._y), 'bottom')

    # Neumann Traction
    traction = np.zeros(spatial_dimension)
    F = 0.095
    traction[int(aka._y)] = F
    model.getExternalForce()[:] = 0
    model.applyBC(aka.FromSameDim(traction), 'top')
    model.solveStep()
    model.dump()
    return model, mesh


def plotMesh(mesh, displacement=None, field=None):
    conn = mesh.getConnectivity(aka._triangle_3)
    nodes = mesh.getNodes()
    if displacement is None:
        triangles = tri.Triangulation(nodes[:, 0], nodes[:, 1], conn)
    else:
        triangles = tri.Triangulation(nodes[:, 0]+displacement[:, 0],
                                      nodes[:, 1]+displacement[:, 1], conn)

    fig = plt.Figure()
    axe = fig.add_subplot(111)
    axe.set_aspect('equal')
    axe.triplot(triangles, '--', lw=.8)
    return fig


def plotResult(model, displacement=None, field=None,
               log_color=False, contour=None,
               contour_label=True,
               xrange=None, yrange=None):
    mesh = model.getMesh()
    conn = mesh.getConnectivity(aka._triangle_3)
    nodes = mesh.getNodes()

    fig = plt.Figure()
    axe = fig.add_subplot(111)
    axe.set_aspect('equal')

    pos = nodes.copy()

    if displacement is not None:
        pos += displacement

    triangles = tri.Triangulation(pos[:, 0], pos[:, 1], conn)
    contour_fmt = None
    if field == 'stress':
        field = model.getMaterial(0).getStress(aka._triangle_3)
        nodal_field = np.zeros((nodes.shape[0], field.shape[1]))
        for el, c in enumerate(conn):
            for n in c:
                nodal_field[n, :] += field[el]/3
        label = 'stress magnitude [Pa]'
        def stress_fmt(x): return "$" + str(round(x, 2)) + "\\quad [Pa]$"
        contour_fmt = stress_fmt

    elif field == 'displacement':
        field = model.getDisplacement()
        label = 'displacement [m]'
        def disp_fmt(x): return "$" + str(round(x, 2)) + "\\quad [m]$"
        contour_fmt = disp_fmt
        nodal_field = field

    color = np.linalg.norm(field, axis=1)
    if log_color:
        color = np.log(color)
    display = axe.tripcolor(triangles, color)

    if xrange is not None:
        axe.set_xlim(xrange)
    if yrange is not None:
        axe.set_ylim(yrange)

    if contour is not None:
        f = np.linalg.norm(nodal_field, axis=1)
        CS = axe.tricontour(triangles, f, contour,
                            linewidths=2, colors='red')
        if contour_label:
            axe.clabel(CS, CS.levels, fmt=contour_fmt, inline=True)
    else:
        cbar = fig.colorbar(display)
        cbar.set_label(label)

    return fig
