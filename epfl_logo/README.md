This tutorial studies the structural stability of the EPFL logo, by applying a gravity field
and performing and explicit time integration. 

![](https://www.economie-region-lausanne.ch/wp-content/uploads/2019/07/epfl-2-870x350.jpg)

A geometric description (with Gmsh syntax) of the *EPFL* logo is created to let
the *Gmsh* mesher generate a triangular mesh loadable with Akantu.
A *Newmark*-$\beta$ time integration is then employed, allowing to produce a video of the 
evolution in time of the logo: it is a *mechanism*.
